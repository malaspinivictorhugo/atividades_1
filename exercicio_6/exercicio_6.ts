//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o salário a receber, sabendo-se que esse funcionário tem gratificação de 5% sobre o salário base e paga imposto de 7% sobre o salário-base.

namespace exercicio_6
{
    let salario_base: number;
    salario_base = 1000

    let imposto: number;
    imposto = salario_base * -7/100

    let bonus: number;
    bonus = salario_base * 5/100

    let salario_final: number; 
    salario_final = salario_base + imposto + bonus;

    console.log(`o salario final é de ${salario_final}`);

}