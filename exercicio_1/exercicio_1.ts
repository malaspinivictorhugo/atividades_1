//Comentar
/*
Faça um programa que receba quatro números inteiros, calcule e mostre a soma desses números 
*/
//primeiro bloco
//Inicio
namespace exercicio_1 
{
    //entrada dos dados 
    //var   let   const
    let numero1: number;
    let numero2: number;
    let numero3: number;
    let numero4: number;
    
    numero1 = 5; 
    numero2 = 10;
    numero3 = 15;
    numero4 = 20;

    let resultado: number;

    //processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida
    console.log("O resultadoda soma é: " + resultado);
    //Ou pode-se escrever
    console.log(`O resultado da soma é: ${resultado}`);
    


}

